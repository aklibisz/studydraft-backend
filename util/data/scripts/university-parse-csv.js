var fs = require('fs'),
	Converter = require('csvtojson').Converter;


var fname = process.argv[2],
	fStream = fs.createReadStream(fname),
	converter = new Converter({
		constructResult: true
	});

converter.on('end_parsed', function(result) {
	console.log(result);
})

fStream.pipe(converter);