var fs = require('fs'),
	Converter = require('csvtojson').Converter;


var fname = process.argv[2],
	fStream = fs.createReadStream(fname),
	converter = new Converter({
		constructResult: true
	});

converter.on('end_parsed', function(result) {

	var universities = [];

	result.forEach(function(r) {

		if (r.ICLEVEL === 1 &&
			r.STABBR == 'TN') {
			var u = {};
			u.formalId = r.UNITID;
			u.name = r.INSTNM;
			u.shortName = r.IALIAS;
			u.locationAddress = r.ADDR;
			u.locationCity = r.CITY;
			u.locationState = r.STABBR;
			u.locationZip = r.ZIP;
			u.locationGeo = [r.LATITUDE, r.LONGITUD];
			universities.push(u);
		}

	})

	console.log(JSON.stringify(universities));


})

fStream.pipe(converter);