var Faker = require('faker'),
	config = require('../_config.mock.json').create.course;

describe('Creating Courses', function() {

	var numCourses = config.number,
		created = 1;

	it('should create ' + numCourses + ' courses', function(done) {

		if (numCourses == 0) done();

		for (var i = 0; i < numCourses; i++) {

			var course = {
				catalogCode: Faker.random.number(),
				number: Faker.random.number() % 400 + 100,
				name: Faker.lorem.words(4).join(' '),
				subject: Faker.lorem.words(1).join().toUpperCase().substr(0, 4),
				professor: Faker.name.findName(),
				term: "Fall",
				year: new Date().getFullYear(),
				university: Faker.random.number()
			}

			Course.create(course)
				.exec(function(error, course) {
					if (!error) {
						console.log(created, course.name);
					} else {
						console.error(error);
					}
					if (created++ == numCourses) {
						done();
					}
				});
		}

	})

})