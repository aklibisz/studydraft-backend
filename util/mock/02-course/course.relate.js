var Faker = require('faker'),
	config = require('../_config.mock.json').create.course;

describe('Relating Courses', function() {

	var numCourses = config.number,
		related = 1;

	it('should relate courses with universities', function(done) {

		University
			.findOne({
				fixedId: 221759
			})
			.then(function(u) {
				Course.find()
					.limit(100)
					.exec(function(error, courses) {
						courses.forEach(function(c) {
							u.courses.add(c);
						});
						u.save(function(error, res) {
							done();
						})
					})
			});
	});

})
