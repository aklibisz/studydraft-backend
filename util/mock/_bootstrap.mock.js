var Sails = require('sails');
require('should');

before(function(done) {
	Sails.lift({
		log: {
			level: 'error'
		},
		models: {
			connection: 'mongo',
			migrate: 'drop'
		}
	}, function(err, sails) {
		if (err) {
			return done(err);
		}
		done(err, sails);
	});
});

after(function(done) {
	sails.lower(done);
});
