var Faker = require('faker'),
	config = require('../_config.mock.json').create.user;

describe('Creating Users', function() {

	var numUsers = config.number,
		created = 1;

	it('should create ' + numUsers + ' users', function(done) {

		// Some default users for testing

		User.create({
			firstName: 'App',
			lastName: 'Tester',
			email: 'test@test.com',
			password: 'test123',
			username: 'appTester'
		}).exec(function(error, user) {
			if(error) {
				console.error(error);
			}
		});

		// More random users

		if(numUsers == 0) done();

		for (var i = 0; i < numUsers; i++) {

			var firstName = Faker.name.firstName(),
				lastName = Faker.name.lastName();

			var user = {
				firstName: firstName,
				lastName: lastName,
				email: firstName + '.' + lastName + '@test.com',
				password: 'test123',
				username: firstName + '.' + lastName,
				university: config.defaults.university || Faker.random.number()
			};

			User.create(user)
				.exec(function(error, user) {
					if (!error) {
						console.log(created, user.firstName + ' ' + user.lastName);
					} else {
						console.error(error);
					}

					if(created++ == numUsers) {
						done();
					}
				});
		}

	})

})
