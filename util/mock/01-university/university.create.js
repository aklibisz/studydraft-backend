var Faker = require('faker'),
	config = require('../_config.mock.json').create.university,
	data = require('./universities-four-year-tn-select.json');

describe('Creating Universities', function() {

	var numUniversities = data.length
		created = 1;

	if(config.number == 0) numUniversities = config.number;

	it('should create ' + numUniversities + ' universities', function(done) {

		if(numUniversities == 0) done();		

		for (var i = 0; i < numUniversities; i++) {

			var university = data[i];
			University.create(university)
				.exec(function(error, university) {
					if (!error) {
						console.log(created, university.name);
					} else {
						console.error(error);
					}
					if (created++ == numUniversities) {
						done();
					}
				});
		}

	})

})