var Faker = require('faker');

describe('Relating Study Teams', function() {

  it('should relate study teams with users', function(done) {

    StudyTeam.find()
      .then(function(studyTeams) {
        var numStudyTeams = studyTeams.length,
          saved = 0;
        studyTeams.forEach(function(studyTeam) {
          var randomLetter = Faker.name.firstName().substr(0, 1);
          User.find({
              firstName: {
                contains: randomLetter
              }
            })
            .limit(5)
            .then(function(users) {
              // studyTeam.users = users;
              users.forEach(function(u) {
                studyTeam.users.add(u);
              });
              return studyTeam.save();
            })
            .then(function(studyTeam) {
              console.log(saved, studyTeam.id);
              if(++saved == numStudyTeams) {
                done();
              }
            });
        });
      });
  });

  it('should relate study teams with study sessions', function(done) {

    StudyTeam.find()
      .populate('users')
      .then(function(studyTeams) {
        var numStudyTeams = studyTeams.length,
          saved = 0;

        studyTeams.forEach(function(studyTeam) {

          // Create 10 new study sessions for each study team
          for(var i = 0; i < 10; i++) {
            var studySession = StudySession.mock(1)[0];

            StudySession.create(studySession)
              .then(function(studySession) {
                // Add all the users from the study team to the study session
                // and save the study session
                studyTeam.users.forEach(function(user) {
                  studySession.users.add(user);
                });
                // Associate the study team as belonging to this study session
                studySession.studyTeams.add(studyTeam);
                return studySession.save();
              })
              .then(function() {
                if(++saved == numStudyTeams) {
                  done();
                }
              });
          }
        });
      });
  });

});
