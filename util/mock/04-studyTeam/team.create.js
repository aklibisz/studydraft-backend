describe('Creating Study Teams', function() {

  var numStudyTeams = 50,
    created = 1;

  it('should create ' + numStudyTeams + ' studyTeams', function(done) {

    if(numStudyTeams == 0) done();

    for(var i = 0; i < numStudyTeams; i++) {
      var studyTeam = StudyTeam.mock();
      StudyTeam.create(studyTeam)
        .then(function(studyTeam) {
          console.log(created, studyTeam);
          if(created++ == numStudyTeams) {
            done();
          }
        });
    }

  });

});
