var Faker = require('faker'),
  Moment = require('moment'),
  config = require('../_config.mock.json').relate.studySession;

describe('Relating Study Sessions', function() {

	it('should relate courses to study sessions', function(done) {
    // Get 100 active study sessions
    StudySession.find({
        where: {
          endingAt: {
            'greaterThan': new Date()
          }
        }
      })
			.limit(50)
      .then(function(studySessions) {
				// Get 100 courses
        var courses = Course
          .find()
          .limit(50);
        return [studySessions, courses];
      })
      .spread(function(studySessions, courses) {
        // Add two courses to each studysession,
        // making sure they are in the same university
        console.log(studySessions.length + ' studySessions', courses.length +  ' courses');
				var associated = 0;
        studySessions.forEach(function(ss) {
          numCourses = Faker.random.number() % 5 + 1;
          for (var i = 0; i < numCourses; i++) {
            var c = courses[Faker.random.number() % courses.length];
						ss.courses.add(c.id);
          }
					ss.save().then(function(ss) {
              console.log(associated, 'saved studySession', ss.id);
            })
            .catch(function(error) {
							console.error(associated, error);
            })
						.finally(function() {
							if (++associated == studySessions.length) done();
						});
        });
      });
  });

  it('should relate study sessions with users', function(done) {

    // Get all active studySessions
    StudySession.find({
        where: {
          endingAt: {
            'greaterThan': new Date()
          }
        }
      })
			.limit(50)
      .then(function(studySessions) {
        // Get 100 users
        var users = User.find().limit(50);
        return [studySessions, users];
      })
      .spread(function(studySessions, users) {
        // Relate five users to each study session
        console.log(studySessions.length + ' studySessions', users.length + ' users');
        var associated = 0;
        studySessions.forEach(function(ss) {
          for (var i = 0; i < 5; i++) {
            var u = users[Faker.random.number() % users.length];
            ss.users.add(u.id);
          }
					ss.save().then(function(ss) {
              console.log(associated, 'saved studySession', ss.id);
            })
            .catch(function(error) {
							console.error(associated, error);
            })
						.finally(function() {
							if (++associated == studySessions.length) done();
						});
        });
      });
  });
});
