var Faker = require('faker'),
  Moment = require('moment'),
  config = require('../_config.mock.json').create.studySession;

describe('Creating Study Sessions', function() {

  var numStudySessions = config.number,
    created = 1;

  it('should create ' + numStudySessions + ' study sessions', function(done) {

    if (numStudySessions == 0) done();

    University.findOne({
        shortName: 'UTK'
      })
      .then(function(university) {
        return StudySession.mock(numStudySessions, university.id);
      })
      .then(function(mockStudySessions) {
        for (var i = 0; i < mockStudySessions.length; i++) {
          StudySession.create(mockStudySessions[i])
            .exec(function(error, studySession) {
              if (!error) {
                console.log(created, studySession.description);
              } else {
                console.error(error);
              }
              if (created++ == numStudySessions) {
                done();
              }
            });
        }
      });
  });
});
