#! /bin/bash
### Use the passed application name and environment name
APPLICATION_NAME=$1
ENVIRONMENT_NAME=$2

### Define necessary variables based off of the application and environment names
DOCKER_IMAGE_NAME=$APPLICATION_NAME
VERSION_TAG=$CIRCLE_BRANCH-$CIRCLE_BUILD_NUM
QUAY_REPO=quay.io/alexklibisz/studydraft-backend:$VERSION_TAG
S3_BUCKET=studydraft-code
DOCKERRUN_FILE=$VERSION_TAG-Dockerrun.aws.json

### Login to Quay.io
docker login -u $SD_QUAY_USER -p $SD_QUAY_PASS -e $SD_QUAY_EMAIL quay.io
### Tag the image built during the Circle CI testing process
docker tag $DOCKER_IMAGE_NAME $QUAY_REPO
### Push the image to Quay.io
docker push $QUAY_REPO

### Setup AWS credentials
touch aws-credentials.txt
echo $SD_AWS_ACCESS_KEY_ID >> aws-credentials.txt
echo $SD_AWS_SECRET_ACCESS_KEY >> aws-credentials.txt
echo $SD_AWS_REGION >> aws-credentials.txt
echo >> aws-credentials.txt
cat aws-credentials.txt | aws configure
rm aws-credentials.txt

### Create a modified Dockerrun.aws.json file for this version
sed "s/<TAG>/$VERSION_TAG/" < Dockerrun.aws.json.template > $DOCKERRUN_FILE
### Copy the file to AWS S3
aws s3 cp $DOCKERRUN_FILE s3://$S3_BUCKET/$APPLICATION_NAME/$DOCKERRUN_FILE
### Create a new Elastic Beanstalk application version
aws elasticbeanstalk create-application-version --application-name $APPLICATION_NAME \
  --version-label $VERSION_TAG --source-bundle S3Bucket=$S3_BUCKET,S3Key=$APPLICATION_NAME/$DOCKERRUN_FILE
### Update the Elastic Beanstalk environment to the new version
aws elasticbeanstalk update-environment --environment-name $ENVIRONMENT_NAME \
  --version-label $VERSION_TAG
