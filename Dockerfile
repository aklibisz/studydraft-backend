FROM node:0.12.7

ENV HOME=/root

RUN apt-get update
RUN apt-get install python2.7 -y

# Install global npm modules,
# may have to use unsafe-perm otherwise it complains about root permissions
RUN npm install -g sails pm2

# Move to the directory we'll be running from
RUN mkdir -p /root/studydraft-api
WORKDIR /root/studydraft-api

# Pull in the application source
ADD . /root/studydraft-api

# Install application packages
RUN npm install --production

# RUN on port 80
EXPOSE 80

# Start the app once to migrate the database
RUN pm2 start .pm2-migrate.json

# Launch the app permanently with pm2
CMD ["pm2", "start", ".pm2-start.json", "--no-daemon"]
