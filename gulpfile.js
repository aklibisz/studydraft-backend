var gulp = require('gulp'),
  eslint = require('gulp-eslint'),
  prettify = require('gulp-jsbeautifier'),
  server = require('gulp-develop-server'),
  env = require('gulp-env'),
  mocha = require('gulp-mocha'),
  gutil = require('gulp-util'),
  bump = require('gulp-bump'),
  filter = require('gulp-filter'),
  shell = require('gulp-shell'),
  tagVersion = require('gulp-tag-version'),
  exec = require('child_process').exec;

var config = {
  src: {
    js: [
      'api/controllers/**/*.js',
      'api/models/**/*.js',
      'api/policies/**/*.js',
      'api/services/**/*.js'
    ]
  },
  dest: './',
  server: {
    path: './app.js'
  }
};

gulp.task('default', ['eslint', 'watch', 'server:start'], function() {});

gulp.task('deploy-dev', function() {
  (shell.task([
    'git checkout deploy-dev',
    'git reset --hard',
    'git merge master',
    'git push -f origin deploy-dev --tags',
    'sleep 5',
    'git checkout master'
  ]))();
});

gulp.task('deploy-prod', function() {
  (shell.task([
    'git checkout deploy-prod',
    'git reset --hard',
    'git merge master',
    'git push -f origin deploy-prod --tags',
    'git checkout master'
  ]))();
});

gulp.task('docker-build', function() {
  var buildCommand = 'docker build -t studydraft-backend .';
  (shell.task(buildCommand))();
})

gulp.task('docker-run', function() {
  var environmentVariables = [
    'SD_CONNECTIONS_MONGO_URL',
    'SD_PORT',
    'SD_ENVIRONMENT',
    'SD_MODELS_MIGRATE',
    'SD_TOKENAUTH_SECRET',
    'SD_EMAIL_AUTH_USER',
    'SD_EMAIL_AUTH_PASS',
    'SD_PUSH_SNS_ACCESS_KEY_ID',
    'SD_PUSH_SNS_SECRET_ACCESS_KEY',
    'SD_PUSH_SNS_ANDROID_ARN'
  ];

  environmentVariables = environmentVariables.map(function(ev) {
    return '-e "' + ev + '=$' + ev + '"';
  }).join(' ');

  var runCommand = [
    'docker run',
    '-d',
    '-p 8000:80',
    environmentVariables,
    'studydraft-backend'
  ].join(' ');

  (shell.task(runCommand))();
})

gulp.task('docker-start', ['docker-build', 'docker-run']);

gulp.task('docker-stop', function() {
  (shell.task([
    'docker stop $(docker ps -a -q)',
    'docker rm $(docker ps -a -q)'
  ]))();
})

gulp.task('eslint', function() {
  gulp.src(config.src.js)
    .pipe(eslint())
    .pipe(eslint.format());
});

gulp.task('server:start', function() {
  server.listen(config.server);
});

gulp.task('server:restart', function() {
  server.restart;
});

/**
 * The base test function, launches gulp mocha. Other
 * test tasks set up the environment for testing.
 * @param  {[type]} 'test'    [description]
 * @param  {[type]} function( [description]
 * @return {[type]}           [description]
 */
gulp.task('test', function() {
  gulp.src(['test/**/*.js'], {
      read: false
    })
    .pipe(mocha({
      reporter: 'spec',
      timeout: '20000'
    }))
    .on('error', gutil.log)
    .once('end', function() {
      process.exit();
    });
});

/**
 * Sets up the environment for local testing - i.e. just
 * launch the sails app and test. Then calls the base test
 * task.
 * @param  {[type]} 'test-local' [description]
 * @param  {[type]} function(    [description]
 * @return {[type]}              [description]
 */
gulp.task('test-local', function() {
  env({
    vars: {
      'TEST_ENVIRONMENT': 'Local Sails App',
      'API_URL': 'http://localhost:1337'
    }
  });
  gulp.start('test');
});

/**
 * Sets up the environment for docker-based testing. Then
 * calls the base test task.
 * @param  {[type]} 'test-docker' [description]
 * @param  {[type]} function(     [description]
 * @return {[type]}               [description]
 */
gulp.task('test-docker', function() {
  env({
    vars: {
      'TEST_ENVIRONMENT': 'Dockerized Sails App',
      'API_URL': 'http://localhost:8000'
    }
  });
  gulp.start('test');
});

/**
 * Sets up the environment to test against the dev server.
 * Then calls the base test task.
 * @param  {[type]} 'test:dev' [description]
 * @param  {[type]} function(  [description]
 * @return {[type]}            [description]
 */
gulp.task('test-dev', function() {
  env({
    vars: {
      'TEST_ENVIRONMENT': 'Dev Server',
      'API_URL': 'https://dev.api.studydraftapp.com'
    }
  });
  gulp.start('test');
});


gulp.task('watch', function() {
  gulp.watch(config.src.js, ['eslint']);
  gulp.watch(config.src.js, server.restart);
});
