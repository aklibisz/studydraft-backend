var SNS = require('sns-mobile'),
  Promise = require('bluebird');

var config = sails.config.push,
  androidApp = new SNS({
    platform: SNS.SUPPORTED_PLATFORMS.ANDROID,
    region: 'us-east-1',
    apiVersion: '2010-03-31',
    accessKeyId: config.sns.accessKeyId,
    secretAccessKey: config.sns.secretAccessKey,
    platformApplicationArn: config.sns.androidArn
  });

module.exports = {

  /**
   * Registers a device with the SNS push notification service.
   *
   * @param  {String} platform 'android' or 'ios'
   * @param  {String} deviceId The unique device id or token generated on the client side
   * @return {Promise}          Wraps the aws sdk call in a promise, resolves with the endpointArn
   */
  registerDevice: function(owner, platform, deviceId) {
    switch (platform) {
      case 'android':
        return new Promise(function(resolve, reject) {
          androidApp.addUser(deviceId, JSON.stringify(owner.email), function(error, endpointArn) {
            if (error) {
              console.error('Could not register device with SNS', error);
              return reject(error);
            }
            return resolve(endpointArn);
          });
        });
        break;
    }
  },

  sendMessage: function(deviceObject, message) {
    switch (deviceObject.platform) {
      case 'android':
        return new Promise(function(resolve, reject) {
          androidApp.sendMessage(deviceObject.endpointArn, message, function(error, messageId) {
            if (error) {
              console.error(error);
              return reject(error);
            }
            return resolve(messageId);
          });
        });
        break;
      case 'ios':
    }
  },

  sendMessageToDevices: function(devices, message) {

    return new Promise(function(resolve, reject) {

      if(!devices || devices.length === 0) return resolve(0);

      var deviceCount = devices.length,
        devicesComplete = 0;

      devices.forEach(function(device) {
        PushService.sendMessage(device, message)
          .then(function() {
            if (devicesComplete++ === deviceCount) {
              return resolve(devicesComplete)
            }
          });
      });
    });
  },

  sendMessageToUsers: function(users, message) {

    if(!Array.isArray(users)) {
      users = [users];
    }

    return new Promise(function(resolve, reject) {
      var userCount = users.length,
        usersComplete = 0;

      users.forEach(function(user) {
        //TODO: this can be made faster by fetching all users at once using their ids
        User.findOne(user.id)
          .populate('devices')
          .then(function(user) {
            PushService.sendMessageToDevices(user.devices, message)
              .then(function() {
                if(usersComplete++ === userCount) {
                  return resolve(usersComplete);
                }
              });
          });
      });

    });
  }

};
