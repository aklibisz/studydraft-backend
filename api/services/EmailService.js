'use strict';

var NodeMailer = require('nodemailer'),
  // Q = require('q'),
  Promise = require('bluebird');

var config = sails.config.email;

var transporter = NodeMailer.createTransport({
  service: config.service,
  auth: {
    user: config.auth.user,
    pass: config.auth.pass
  }
});

module.exports = {

  sendEmail: function(emailOptions) {
    var email = {
      from: emailOptions.from || config.options.from,
      to: emailOptions.to,
      subject: emailOptions.subject || config.options.subject,
      text: emailOptions.text || config.options.text
    };
    return new Promise(function(resolve, reject) {
      transporter.sendMail(email, function(error, info) {
        if(error) {
          reject(error);
        } else {
          console.log(info);
          resolve(info);
        }
      });
    });
  }
};
