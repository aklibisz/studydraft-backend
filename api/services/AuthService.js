'use strict';

var Q = require('q'),
  Promise = require('bluebird'),
  Bcrypt = require('bcrypt'),
  RandomString = require('random-string'),
  Jwt = require('jsonwebtoken');

var tokenAuthConfig = sails.config.tokenAuth;

module.exports = {

  hashPassword: function(password) {
    return new Promise(function(resolve, reject) {
      Bcrypt.genSalt(10, function(error, salt) {
        Bcrypt.hash(password, salt, function(error, hash) {
          if(error) {
            reject(error);
          } else {
            resolve(hash);
          }
        });
      });
    });
  },

  randomPassword: function(options) {
    options = options || {
      length: 10,
      special: true
    };
    return RandomString(options);
  },

  comparePassword: function(password, validPassword) {
    return new Promise(function(resolve, reject) {
      Bcrypt.compare(password, validPassword, function(error, response) {
        console.log(error, response);
        if(error) {
          reject(error);
        } else if(!response) {
          reject(new Error('Invalid password'));
        } else {
          resolve(response);
        }
      });
    });
  },

  signToken: function(userId) {
    return Jwt.sign(userId, tokenAuthConfig.secret, {
      expiresInMinutes: tokenAuthConfig.expiresInMinutes
    });
  },

  verifyToken: function(token) {
    var dfr = Q.defer();
    Jwt.verify(token, tokenAuthConfig.secret, function(error, decoded) {
      if(error) {
        dfr.reject(error);
      } else {
        dfr.resolve(decoded);
      }
    });
    return dfr.promise;
  }
};
