'use strict';

var FB = require('fb'),
  Promise = require('bluebird');

module.exports = {
  FB: FB,
  facebook: {
    sdk: FB,
    getUserInformation: function(fbAccessToken) {
      return new Promise(function(resolve, reject) {
        FB.api('me/', {
					//TODO: make this a parameter
          'fields': 'id,first_name,last_name,email,gender,picture',
          'access_token': fbAccessToken
        }, function(fbResponse) {
          if (fbResponse.error) {
            reject(fbResponse.error);
          } else {
            resolve(fbResponse);
          }
        });
      });
    }
  }
};
