/**
 * Alex Klibisz, aklibisz@utk.edu
 *
 * 7/16/2015
 *
 * This is a policy used to verify the requestuest token passed
 * for a requestuest to the API.
 */
'use strict';

module.exports = function(request, response, next) {

  var accessToken = request.headers.sdaccesstoken || request.query.sdaccesstoken;

  AuthService.verifyToken(accessToken)
    .then(function(decoded) {
			// Check if the user exists
      User.findOne(decoded)
        .then(function(user) {
          // Attach the user's id to the request and continue
          request.options.userId = user.id;
          return next();
        })
        .catch(function(error) {
					// User doesn't exist. Return error.
          return response.forbidden({
            summary: 'TokenAuth: token could not be match to a user',
            body: error
          });
        });
    })
		.catch(function(error) {
      // Token could not be verified
			return response.forbidden({
        summary: 'TokenAuth: invalid token; you are not permitted',
        body: error
      });
    });
};
