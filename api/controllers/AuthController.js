/**
 * AuthController
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
'use strict';

var passport = require('passport');

module.exports = {

    _config: {
        actions: false,
        shortcuts: false,
        rest: false
    },

    login: function(request, response) {

        passport.authenticate('local', function(error, user) {
            if (error) {
                return response.badRequest({
                    summary: 'Auth: error',
                    body: error
                });
            }
            if (!user) {
                return response.notFound({
                    summary: 'Auth: user not found, email or password is incorrect',
                    body: null
                });
            }
            request.logIn(user, function(error) {
                if (error) {
                    return response.forbidden({
                        summary: 'Auth: login error.',
                        body: error
                    });
                }
                // Generate token and attach to return object.
                return response.ok({
                    summary: 'Auth: logged in successfully.',
                    user: user,
                    sdaccesstoken: AuthService.signToken(user.id)
                });
            });

        })(request, response);
    },

    registerFacebook: function(request, response) {
      var fbToken = request.body.accessToken;
      User.registerFacebook(fbToken)
        .then(function(user) {
          return response.ok({
              summary: 'Auth: registered with Facebook successfully',
              user: user,
              sdaccesstoken: AuthService.signToken(user.id)
          });
        })
        .catch(function(error) {
          console.error(error);
          return response.badRequest({
            summary: 'Auth: could not complete Facebook registration',
            body: error
          });
        });
    },

    loginFacebook: function(request, response) {
      var fbToken = request.body.accessToken;
      User.loginFacebook(fbToken)
        .then(function(user) {
          return response.ok({
              summary: 'Auth: logged in with Facebook successfully',
              user: user,
              sdaccesstoken: AuthService.signToken(user.id)
          });
        })
        .catch(function(error) {
          return response.badRequest({
            summary: 'Auth: ' + (error.summary || 'could not complete Facebook login'),
            body: error
          });
        });
    }
};
