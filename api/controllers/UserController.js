/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
'use strict';

module.exports = {

  associate: function(request, response) {

    var userId = request.params.id,
      property = request.body.property,
      propertyIds = request.body.propertyIds;

    if (!userId || !property || !propertyIds) {
      return response.badRequest({
        summary: 'User: could not associate because parameters are missing',
        body: null
      });
    }

    User.associate(userId, property, propertyIds)
      .then(function(user) {
        return response.ok(user);
      })
      .catch(function(error) {
        return response.badRequest({
          summary: 'User: could not associate',
          body: error
        });
      });
  },

  changePassword: function(request, response) {
    var userId = request.params.id,
      currentPassword = request.body.currentPassword,
      newPassword = request.body.newPassword,
      newPasswordConfirm = request.body.newPasswordConfirm;

    if (!userId || !currentPassword || !newPassword || !newPasswordConfirm) {
      return response.badRequest({
        summary: 'User: cannot change password, required credentials are missing',
        body: null
      });
    }

    User.changePassword(userId, currentPassword, newPassword, newPasswordConfirm)
      .then(function(user) {
        return response.ok(user);
      })
      .catch(function(error) {
        return response.badRequest({
          summary: 'User: ' + (error.summary || 'cannot change password'),
          error: error
        });
      });
  },

  disassociate: function(request, response) {

    var userId = request.params.id,
      property = request.body.property,
      propertyIds = request.body.propertyIds;

    if (!userId || !property || !propertyIds) {
      return response.badRequest({
        summary: 'User: could not disassociate because parameters are missing',
        body: null
      });
    }

    User.disassociate(userId, property, propertyIds)
      .then(function(user) {
        return response.ok(user);
      })
      .catch(function(error) {
        return response.badRequest({
          summary: 'User: could not disassociate',
          body: error
        });
      });
  },

  me: function(request, response) {

    var id = request.options.userId;
    User.findOne({
        id: id
      })
      .then(function(user) {
        return response.ok(user);
      })
      .catch(function(error) {
        return response.badRequest({
          summary: 'User: error',
          body: error
        });
      });
  },

  resetPassword: function(request, response) {
    var email = request.body.email;

    if (!email || email.length === 0) {
      return response.badRequest({
        summary: 'User: email is required to reset password',
        body: null
      });
    }

    User.resetPassword(email)
      .then(function(info) {
        return response.ok(info);
      })
      .catch(function(error) {
        return response.badRequest({
          summary: 'User: password reset failed',
          body: error
        });
      });

  },

  search: function(request, response) {
    
    User.search(request.query)
      .then(function(users) {
        return response.ok(users);
      })
      .catch(function(error) {
        return response.badRequest({
          summary: 'User: could not complete requested search',
          body: error
        });
      });
  },

  syncFacebook: function(request, response) {
    var userId = request.params.id,
      fbToken = request.body.accessToken;

    User.syncFacebook(userId, fbToken)
      .then(function(user) {
        return response.ok(user);
      })
      .catch(function(error) {
        return response.badRequest({
          summary: 'User: could not connect or sync Facebook account',
          body: error
        });
      });

  }

};