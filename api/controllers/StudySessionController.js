/**
 * StudySessionController
 *
 * @description :: Server-side logic for managing Studysessions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
'use strict';

module.exports = {

  associate: function(request, response) {

    var studySessionId = request.params.id,
      property = request.body.property,
      propertyIds = request.body.propertyIds;

    if (!studySessionId || !property || !propertyIds) {
      return response.badRequest({
        summary: 'StudySession: could not associate because parameters are missing',
        body: null
      });
    }

    StudySession.associate(studySessionId, property, propertyIds)
      .then(function(studySession) {
        return response.ok(studySession);
      })
      .catch(function(error) {
        return response.badRequest({
          summary: 'StudySession: could not associate',
          body: error
        });
      });
  },

  disassociate: function(request, response) {

    var studySessionId = request.params.id,
      property = request.body.property,
      propertyIds = request.body.propertyIds;

    if (!studySessionId || !property || !propertyIds) {
      return response.badRequest({
        summary: 'StudySession: could not disassociate because parameters are missing',
        body: null
      });
    }

    StudySession.disassociate(studySessionId, property, propertyIds)
      .then(function(studySession) {
        return response.ok(studySession);
      })
      .catch(function(error) {
        return repsonse.badRequest({
          summary: 'StudySession: could not disassociate',
          body: error
        });
      });
  },

  joinRequestSubmit: function(request, response) {

    var studySessionId = request.params.id,
      requestingUserId = request.options.userId;

    if(!studySessionId || !requestingUserId) {
      return response.badRequest({
        summary: 'StudySession: could not request to join because parameters are missing',
        body: null
      });
    }

    return User.findOne(requestingUserId)
      .then(function(requestingUser) {
          return StudySession.joinRequestSubmit(studySessionId, requestingUser)
            .then(function(studySession) {
              return response.ok(studySession);
            })
            .catch(function(error) {
              return response.badRequest({
                summary: 'StudySession: could not process join request',
                body: null
              });
            });
      })

  },

  joinRequestAccept: function(request, response) {

  },

  joinRequestReject: function(request, response) {

  }

};
