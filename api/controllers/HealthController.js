/**
 * HealthController
 *
 * @description :: Server-side logic for managing application Health
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
'use strict';

module.exports = {

  check: function(request, response) {

    return response.ok('OK');

  }

};
