/**
 * DeviceController
 *
 * @description :: Server-side logic for managing Devices
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	/**
	 * Registers a user's device. For example, this endpoint may be hit after
	 * a user has logged into the application.
	 * @param  {[type]} request  [description]
	 * @param  {[type]} response [description]
	 * @return {[type]}          [description]
	 */
	register: function(request, response) {

		var owner = request.body.owner,
			platform = request.body.platform,
			deviceId = request.body.deviceId;

		// Error check
		if(!owner || !platform || !deviceId) {
			return response.badRequest({
				summary: 'Device: could not complete device registration, missing parameters',
				body: null
			});
		}

		// Execute the registration
		Device.register(owner, platform, deviceId)
			.then(function(device) {
				PushService.sendMessage(device, 'Your device has been registered!');
				return response.ok(device);
			})
			.catch(function(error) {
				return response.badRequest({
					summary: 'Device: could not complete device registration',
					body: error
				});
			});
	}

};
