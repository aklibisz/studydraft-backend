/**
 * StudySession.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
'use strict';

var Faker = require('faker'),
  Moment = require('moment');

module.exports = {

  schema: true,

  attributes: {

    createdBy: {
      model: 'User'
    },

    startingAt: {
      type: 'datetime',
      defaultsTo: new Date()
    },

    endingAt: {
      type: 'datetime',
      required: false
    },

    tags: {
      type: 'array',
      required: false
    },

    description: {
      type: 'text',
      required: false
    },

    locationDescription: {
      type: 'string',
      required: false
    },

    // locationGeo: {
    //   type: 'array',
    //   required: false
    // },

    maxUsers: {
      type: 'integer',
      required: false
    },

    visibility: {
      type: 'String',
      enum: ['StudyBoard', 'StudyTeam'],
      required: true,
      defaultsTo: 'StudyBoard'
    },

    university: {
      model: 'University',
      required: true
    },

    courses: {
      collection: 'Course',
      via: 'studySessions'
    },

    users: {
      collection: 'User',
      via: 'studySessions'
    },

    studyTeams: {
      collection: 'StudyTeam',
      via: 'studySessions'
    }

  },

  associate: function(id, property, propertyIds) {
    if (!Array.isArray(propertyIds)) {
      propertyIds = propertyIds.split();
    }
    // Find the studySession
    return StudySession.findOne(id)
      .then(function(studySession) {
        // Add the model ids to the studysession's property
        propertyIds.forEach(function(id) {
          studySession[property].add(id);
        });
        // Save the studySession
        return studySession.save();
      })
      .then(function(studySession) {
        // Return the studysession with the property populated
        return StudySession
          .findOne(studySession.id)
          .populate(property);
      });
  },

  disassociate: function(id, property, propertyIds) {
    if(!Array.isArray(propertyIds)) {
      propertyIds = propertyIds.split();
    }
    return StudySession.findOne(id)
      .then(function(studySession) {
        // Remove the model ids from the studysession's property
        propertyIds.forEach(function(id) {
          studySession[property].remove(id);
        });
        // Save the studySession
        return studySession.save();
      })
      .then(function(studySession) {
        // Return the studysession with the property populated
        return StudySession
          .findOne(studySession.id)
          .populate(property);
      });
  },

  joinRequestSubmit: function(id, requestingUser) {

    // Get the users in the study session
    return StudySession.findOne(id)
      .populate('users')
      .then(function(studySession) {
        var message = requestingUser.name() + ' has requested to join your study session (@' + studySession.locationDescription + ').';
        PushService.sendMessageToUsers(studySession.users, message);
      });

  },

  mock: function(number, university) {
    var studySessions = [],
      descriptions = [
        'Studying hard! No goofballs allowed!',
        'Big test tomorrow!', 'Doing work boi!',
        'Bring food we\'re very hungry!',
        'Working on the project due next week',
        'Studying with your mom'
      ],
      tags = [
        '#Math', '#Calc', '#ComputerScience',
        '#English', '#Paper', '#Homework', '#Exam',
        '#Cramming', '#Engineering', '#Physics',
        '#Business', '#Accounting', '#Test', '#Quiz',
        '#Reading'
      ],
      locationDescriptions = [
        'Library Commons', 'Student Union', 'Min Kao',
        'Ayres', 'Library Starbucks', 'PCB', 'Reese',
        'Neyland', 'AMB', 'Library 3rd Floor',
        'Library 2nd Floor'
      ];

    function randomArrayElement(arr) {
      var index = Math.floor(Math.random() * arr.length);
      return arr[index];
    }

    for (var i = 0; i < number; i++) {
      var studySession = {
        description: randomArrayElement(descriptions),
        startingAt: new Date(),
        endingAt: Moment().add(Faker.random.number() % 360 + 60, 'minutes').toDate(),
        tags: [randomArrayElement(tags), randomArrayElement(tags)],
        locationDescription: randomArrayElement(locationDescriptions),
        maxUsers: Faker.random.number() % 10 + 3,
        university: university || 221759
      };
      studySessions.push(studySession);
    }

    return studySessions;

  }
};
