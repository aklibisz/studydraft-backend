/**
 * Device.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

var Promise = require('bluebird');

module.exports = {

  // Using the deviceId as the primary key, as only
  // one user can ever be registered per device.
  autoPK: false,

  attributes: {

    /**
     * The user that owns the device
     * @type {User Object}
     */
    owner: {
      model: 'User'
    },

    /**
     * The platform of the device, either ios or android
     * @type {String}
     */
    platform: {
      type: 'string',
      enum: ['ios', 'android']
    },

    /**
     * The device id or token that is generated on the client side
     * @type {String}
     */
    deviceId: {
      type: 'string',
      required: true,
      primaryKey: true
    },

    /**
     * The AWS SNS ARN endpoint, called a "Platform Endpoint" on SNS
     * @type {String}
     */
    endpointArn: {
      type: 'string',
      required: true
    }

  },

  /**
   * Registers a device. For example, this function may be called from the
   * device register endpoint.
   *
   * First checks if the device already exists. If so, verifies that the owner
   * has not changed, and updates the owner if there is a discrepancy.
   *
   * If the device doesn't already exist, it calls the PushService to register
   * the device with SNS or any other providers. This returns an endpointArn
   * which is used to create the device and store it in the database.
   *
   * @param  {User Object} owner    The user that owns this device
   * @param  {String} platform 'android' or 'ios'
   * @param  {String} deviceId The device id or token that is generated on the client side
   * @return {Promise}          Promise that resolves to a device object
   */
  register: function(owner, platform, deviceId) {

    console.log(owner, platform, deviceId);

    // Check if a device with the same id already exists
    return Device.findOne({
      deviceId: deviceId
    }).then(function(device) {

      if (device) {
        // This device exists already, verify that it's the same user
        // TODO: there is still the issue that the owner data is not
        // updated on SNS. Not a big deal, but something worth fixing eventually.
        if (device.owner === owner.id) {
          return Promise.resolve(device);
        } else {
          device.owner = owner;
          return device.save();
        }
      } else {
        // This device doesn't exist yet, register it via the PushService
        return PushService.registerDevice(owner, platform, deviceId)
          .then(function(endpointArn) {
            var device = {
              owner: owner,
              platform: platform,
              deviceId: deviceId,
              endpointArn: endpointArn
            };
            return Device.create(device);
          });
      }
    });
  }
};
