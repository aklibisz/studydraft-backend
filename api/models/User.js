/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
'use strict';

var Promise = require('bluebird'),
  Faker = require('faker');

module.exports = {

  schema: true,

  attributes: {
    email: {
      type: 'email',
      required: true,
      unique: true
    },

    password: {
      type: 'string',
      minLength: 6,
      required: true
    },

    firstName: {
      type: 'string',
      required: true
    },

    lastName: {
      type: 'string',
      required: true
    },

    name: function() {
      return this.firstName + ' ' + this.lastName;
    },

    nameDenormalized: {
      type: 'string'
    },

    phoneNumber: {
      type: 'string',
      defaultsTo: '0000000000'
    },

    gender: {
      type: 'string'
    },

    emailVerified: {
      type: 'boolean'
    },

    facebookId: {
      type: 'string',
      unique: true
    },

    pictureUrl: {
      type: 'string'
    },

    devices: {
      collection: 'Device',
      via: 'owner'
    },

    university: {
      model: 'University'
    },

    courses: {
      collection: 'Course',
      via: 'users'
    },

    studySessions: {
      collection: 'StudySession',
      via: 'users'
    },

    studySessionsCreated: {
      collection: 'StudySession',
      via: 'createdBy'
    },

    studyTeams: {
      collection: 'StudyTeam',
      via: 'users'
    },

    toJSON: function() {
      var obj = this.toObject();
      // Delete password
      delete obj['password'];

      // Create name property
      obj.name = obj.firstName + ' ' + obj.lastName;

      // Create shortName property
      obj.shortName = obj.firstName + ' ' + obj.lastName.substr(0,1);

      return obj;
    }
  },

  /**
   * Associate the user with another model defined
   * as the passed property and using the propertyIds.
   * @param  {Number} id          The user's id
   * @param  {String} property    Name of the property at which the association lives.
   * @param  {String} propertyIds Comma-separated string of ids to which to associate the user.
   * @return {Promise}            User save promise
   */
  associate: function(id, property, propertyIds) {
    if (!Array.isArray(propertyIds)) {
      propertyIds = propertyIds.split();
    }
    // Find the user
    return User.findOne(id)
      .then(function(user) {
        // Add the model ids to the user's courses
        propertyIds.forEach(function(id) {
          user[property].add(id);
        });
        // Save the user
        return user.save();
      })
      .then(function(user) {
        // Return the user with the property populated
        return User
          .findOne(user.id)
          .populate(property);
      });
  },

  beforeCreate: function(user, next) {
    user = User.denormalize(user);
    AuthService.hashPassword(user.password)
      .then(function(password) {
        user.password = password;
        next();
      })
      .catch(function(error) {
        next(error);
      });
  },

  beforeUpdate: function(user, next) {
    user = User.denormalize(user);
    next();
  },

  changePassword: function(id, currentPassword, newPassword, newPasswordConfirm) {

    // Check if the new passwords are equal
    if (newPassword !== newPasswordConfirm) {
      return Promise.reject({
        summary: 'New password and new password confirmation do not match'
      });
    }

    // Find the user and check if the current password is valid
    return User.findOne(id)
      .then(function(user) {
        if (!user) {
          return Promise.reject({
            summary: 'no matching user found'
          });
        }
        return user;
      })
      .then(function(user) {
        return [user, AuthService.comparePassword(currentPassword, user.password)];
      })
      .spread(function(user) {
        return [user, AuthService.hashPassword(newPassword)];
      })
      .spread(function(user, hashedPassword) {
        user.password = hashedPassword;
        return user.save();
      })
      .catch(function(error) {
        error.summary = 'something went wrong in the password change sequence';
        return Promise.reject(error);
      });

  },

  denormalize: function(user) {
    user.nameDenormalized = (user.firstName + ' ' + user.lastName).toLowerCase();
    return user;
  },

  disassociate: function(id, property, propertyIds) {

    if (!Array.isArray(propertyIds)) {
      propertyIds = propertyIds.split();
    }

    return User.findOne(id)
      .then(function(user) {
        propertyIds.forEach(function(id) {
          user[property].remove(id);
        });
        // Save the user
        return user.save();
      })
      .then(function(user) {
        // Return the user with the property populated
        return User
          .findOne(user.id)
          .populate(property);
      });

  },

  /**
   * User wants to login using his/her facebook account.
   * The user's StudyDraft account should already exist.
   * Get the user's facebookId from the Facebook API, then
   * find and return the user corresponding to that facebook id.
   * @param  {[type]} fbAccessToken [description]
   * @return {[type]}               [description]
   */
  loginFacebook: function(fbAccessToken) {
    // Call the FB API to get user information
    return SocialService.facebook.getUserInformation(fbAccessToken)
      .then(function(response) {
        // Find and return the user with the returned facebook Id
        return User.findOne({
            facebookId: response.id
          })
          .then(function(user) {
            if (!user) {
              return Promise.reject({
                summary: 'no StudyDraft user found for this Facebook account'
              });
            }
            return user;
          });
      });
  },

  mock: function() {

    var firstName = Faker.name.firstName(),
      lastName = new Date().getTime().toString(),
      mockUser = {
        firstName: firstName,
        lastName: lastName,
        email: firstName + '-' + lastName + '@test.com',
        phoneNumber: Faker.phone.phoneNumber(),
        password: AuthService.randomPassword({
          length: 20
        })
      };

    return mockUser;
  },

  /**
   * User wants to create an account using his/her Facebook account.
   * Pull in user information from FB api and create the user.
   * @param  {String} fbAccessToken The token received after logging into facebook.
   * @return {Promise}              Bluebird promise.
   */
  registerFacebook: function(fbAccessToken) {
    // Call the FB API to get user information.
    return SocialService.facebook.getUserInformation(fbAccessToken)
      // Use the returned information to create a new user
      .then(function(response) {
        var newUser = {
          firstName: response.first_name,
          lastName: response.last_name,
          email: response.email,
          facebookId: response.id,
          gender: response.gender,
          password: AuthService.randomPassword({
            length: 20,
            special: true
          }),
          pictureUrl: response.picture.data.url
        };
        return User.create(newUser);
      });
  },

  /**
   * Generate a random password;
   * hash the password and attach to user;
   * save the user;
   * email the new password to the user.
   * Uses the EmailService to email the password
   * @param  {string} email Email address for the user whose password we're reseting.
   * @return {promise}      Standard Promise
   */
  resetPassword: function(email) {
    return User.findOne({
        email: email
      })
      .then(function(user) {
        var newPassword = AuthService.randomPassword({
          length: 10,
          numeric: true,
          letters: false,
          special: false
        });
        return AuthService.hashPassword(newPassword)
          .then(function(hashedPassword) {
            user.password = hashedPassword;
            return user.save();
          })
          .then(function(user) {
            return EmailService.sendEmail({
              to: user.email,
              subject: 'Password Reset',
              text: 'Your temporary password is: \n' + newPassword + '\nYou will be promted to change your password the next time you log in. \n\n - StudyDraft Team'
            });
          });
      });
  },

  search: function(query) {
    var find = {};
    if (query.name) {
      find.nameDenormalized = {
        contains: query.name.toLowerCase()
      };
    }
    if (query.university) {
      find.university = query.university
    }
    return User.find(find)
      .sort('nameDenormalized ASC');
  },

  /**
   * User wants to connect his/her existing account to a Facebook account.
   * Pull in user information from FB api and update the existing user.
   * @param  {Number} id            The user's studydraft id.
   * @param  {String} fbAccessToken The token received after logging into facebook.
   * @return {Promise}              Bluebird promise.
   */
  syncFacebook: function(id, fbAccessToken) {
    return SocialService.facebook.getUserInformation(fbAccessToken)
      .then(function(response) {
        return User.findOne(id)
          .then(function(user) {
            user.facebookId = response.id;
            user.gender = response.gender;
            user.pictureUrl = response.picture.data.url;
            return user.save();
          });
      });
  }

};
