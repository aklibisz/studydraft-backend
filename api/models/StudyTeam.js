/**
 * StudyTeam.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
'use strict';

var Faker = require('faker');

module.exports = {

  attributes: {

    name: {
      type: 'string',
      required: true
    },

    description: {
      type: 'text'
    },

    users: {
      collection: 'User',
      via: 'studyTeams'
    },

    studySessions: {
      collection: 'StudySession',
      via: 'studyTeams'
    }

  },

  mock: function() {

    var studyTeam = {
      name: Faker.lorem.words(3).join(' '),
      description: Faker.lorem.words(12).join(' '),
      users: [],
      studySessions: []
    };

    return studyTeam;
  }
};
