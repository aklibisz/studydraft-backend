/**
 * University.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
'use strict';

module.exports = {

    attributes: {

        fixedId: {
            type: 'integer',
            required: true
        },

        name: {
            type: 'string',
            required: true
        },

        shortName: {
            type: 'string'
        },

        locationAddress: {
            type: 'string'
        },

        locationCity: {
            type: 'string',
            required: true
        },

        locationState: {
            type: 'string',
            required: true
        },

        locationZip: {
            type: 'integer',
            required: true
        },

        locationGeo: {
            type: 'array',
            required: false
        },

        users: {
            collection: 'User',
            via: 'university'
        },

        courses: {
            collection: 'Course',
            via: 'university'
        }
    }
};
