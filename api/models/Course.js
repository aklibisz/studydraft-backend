/**
 * Course.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
'use strict';

module.exports = {

  attributes: {

    catalogCode: {
      type: 'integer',
      required: true
    },

    name: {
      type: 'string',
      required: true
    },

    number: {
      type: 'integer',
      required: true
    },

    subject: {
      type: 'string',
      required: true
    },

    professor: {
      type: 'string',
      required: true
    },

    term: {
      type: 'string',
      required: true,
      enum: ['Fall', 'Spring', 'Summer']
    },

    year: {
      type: 'integer',
      required: true
    },

    university: {
      model: 'University',
      required: true
    },

    users: {
      collection: 'User',
      via: 'courses'
    },

    studySessions: {
      collection: 'StudySession',
      via: 'courses'
    },

    toJSON: function() {
      var obj = this.toObject();
      obj.shortName = obj.subject + ' ' + obj.number;
      return obj;
    }
  }
};
