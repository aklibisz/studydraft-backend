var Sails = require('sails');

before(function(done) {

	// Launch the Sails server to use for testing endpoints
	Sails.lift({
		log: {
			level: 'error'
		},
		models: {
			connection: 'mongo',
			migrate: 'alter'
		}
	}, function(err, sails) {
		if (err) return done(err);

		// Global test object
		sails.config.test = {};

		// Defined here to preserve the un-hashed password
		sails.config.test.currentUser = User.mock();

		// Create the user to be available for all tests
		return User
			.create(sails.config.test.currentUser)
			.then(function(user) {
				// The mocked user has no id, so we add the id here
				sails.config.test.currentUser.id = user.id;
				console.log('test user', sails.config.test.currentUser);
				return AuthService.signToken(user.id)
			})
			// Create the token to be available for all tests
			.then(function(token) {
				sails.config.test.accessToken = token;
				console.log('test accessToken', sails.config.test.accessToken);
				return done(err, sails);
			});
	});
});

after(function(done) {
	sails.lower(done);
});
