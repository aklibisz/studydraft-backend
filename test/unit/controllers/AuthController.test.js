var Request = require('supertest'),
	Faker = require('faker'),
	RandomString = require('random-string');
require('should');

Request = Request(process.env.API_URL);

describe('Auth Routes', function() {

	describe('POST /auth/register', function() {
		it('should register a new user', function(done) {

			var newUser = User.mock();

			function validBody(res) {
				var body = res.body;
				body.should.have.property('id');
				body.firstName.should.equal(newUser.firstName);
				body.lastName.should.equal(newUser.lastName);
				body.email.should.equal(newUser.email);
				body.phoneNumber.should.equal(newUser.phoneNumber);
			}

			Request
				.post('/auth/register')
				.send(newUser)
				.expect(201)
				.expect(validBody)
				.end(done);
		});
	});

	describe('POST /auth/login', function() {
		it('should log the user in', function(done) {

			var newUser = sails.config.test.currentUser;

			function validBody(res) {
				var body = res.body;
				body.user.should.have.property('id');
				body.user.firstName.should.equal(newUser.firstName);
				body.user.lastName.should.equal(newUser.lastName);
				body.user.email.should.equal(newUser.email);
				body.user.phoneNumber.should.equal(newUser.phoneNumber);
				body.should.have.property('sdaccesstoken');
			}

			Request
				.post('/auth/login')
				.send({
					email: newUser.email,
					password: newUser.password
				})
				.expect(200)
				.expect(validBody)
				.end(done);

		});
	});

	// describe('POST /auth/facebook', function() {
	// 	it('should log the user in using Facebook', function(done) {
	// 		console.log('TODO');
	// 		done();
	// 	});
	// });

});
