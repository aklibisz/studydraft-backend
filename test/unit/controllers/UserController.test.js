var Request = require('supertest'),
  Q = require('q'),
  Faker = require('faker');
require('should');

Request = Request(process.env.API_URL);

describe('User Routes', function() {

  describe('GET /user', function() {
    it('should return an array of users', function(done) {

      function validBody(res) {
        var body = res.body;
        body.should.be.an.instanceOf(Array);
      }

      Request
        .get('/user')
        .set({
          sdaccesstoken: sails.config.test.accessToken
        })
        .expect(200)
        .expect(validBody)
        .end(done);
    });
  });

  describe('GET /user/me', function() {
    it('should return the currently logged in user', function(done) {

      function validBody(res) {
        var user = sails.config.test.currentUser;
        res.body.id.should.equal(user.id);
        res.body.firstName.should.equal(user.firstName);
        res.body.lastName.should.equal(user.lastName);
        res.body.email.should.equal(user.email);
        res.body.phoneNumber.should.equal(user.phoneNumber);
      }

      // Request(sails.hooks.http.app)
      Request
        .get('/user/me')
        .set({
          sdaccesstoken: sails.config.test.accessToken
        })
        .expect(200)
        .expect(validBody)
        .end(done);

    });
  });

  describe('POST /user/:id', function() {
    it('should update the currently logged in user', function(done) {

      var user = sails.config.test.currentUser;
      user.lastName += ' modified';

      function validBody(res) {
        res.body.firstName.should.equal(user.firstName);
        res.body.lastName.should.equal(user.lastName);
      }

      // Request(sails.hooks.http.app)
      Request
        .post('/user/' + sails.config.test.currentUser.id)
        .set({
          sdaccesstoken: sails.config.test.accessToken
        })
        .send(user)
        .expect(200)
        .expect(validBody)
        .end(done);

    });
  });

  describe('DELETE /user/:id', function() {
    it('should delete the currently logged in user', function(done) {

      console.log("TODO");
      done();

    });
  });

  describe('POST /user/:id/associate', function() {
    it('should associate courses with a user', function(done) {

      function getCourseIds() {
        return Course.find()
          .limit(10)
          .then(function(courses) {
            return courses.map(function(c) {
              return c.id;
            });
          });
      }

      getCourseIds()
        .then(function(courseIds) {
          Request
            .post('/user/' + sails.config.test.currentUser.id + '/associate')
            .set({
              sdaccesstoken: sails.config.test.accessToken
            })
            .send({
              property: 'courses',
              propertyIds: courseIds
            })
            .expect(200)
            .end(done);
        });
    });
  })

  describe('POST /user/:id/disassociate', function() {
    it('should disassociate the user from a course', function(done) {
      var user = sails.config.test.currentUser;

      function validBody(res) {
        res.body.courses.length.should.be.below(1);
      }

      User
        .findOne(user.id)
        .populate('courses')
        .then(function(user) {

          courseIds = user.courses.map(function(c) {
            return c.id
          });

          Request
            .post('/user/' + user.id + '/disassociate')
            .set({
              sdaccesstoken: sails.config.test.accessToken
            })
            .send({
              property: 'courses',
              propertyIds: courseIds
            })
            .expect(200)
            .expect(validBody)
            .end(done);
        });
    });
  });

  describe('POST /user/changepassword', function() {
    it('should change the user password, then log back in', function(done) {

      console.log('TODO');
      done();

      // Find the user
      // User.findOne(sails.config.test.currentUser.id)
      // 	.then(function(user) {
      // 		// Store the current password and create a new one
      // 		var currentPassword = user.password,
      // 			newPassword = Faker.internet.password();
      // 		return [currentPassword, newPassword]
      // 	})
      // 	.spread(function(currentPassword, newPassword) {
      // 		// Hit the change password endpoint to change the password
      // 		return [newPassword, Request(sails.hooks.http.app)
      // 			.post('/user/' + sails.config.test.currentUser.id + '/changepassword')
      // 			.set({
      // 				sdaccesstoken: sails.config.test.accessToken
      // 			})
      // 			.send({
      // 				currentPassword: currentPassword,
      // 				newPassword: newPassword,
      // 				newPasswordConfirm: newPassword
      // 			})
      // 			.expect(200)];
      // 	})
      // 	.spread(function(newPassword, response) {
      // 		// Hit the login endpoint to verify the changed password
      // 		return Request(sails.hooks.http.app)
      // 			.post('/auth/login')
      // 			.send({
      // 				email: sails.config.test.currentUser.email,
      // 				password: newPassword
      // 			})
      // 			.expect(200)
      // 			.end(done);
      // 	});
    });
  });

  describe('GET /user/search', function() {
    it('should search for and find a user by firstName', function(done) {

      var user = sails.config.test.currentUser;

      function validBody(res) {
        res.body.length.should.be.above(0);
      }

      Request
        .get('/user/search?name=' + user.firstName)
        .set({
          sdaccesstoken: sails.config.test.accessToken
        })
        .expect(200)
        .expect(validBody)
        .end(done);

    });

    it('should search for and find a user by lastName', function(done) {

      var user = sails.config.test.currentUser;

      function validBody(res) {
        res.body.length.should.be.above(0);
      }

      Request
        .get('/user/search?name=' + user.lastName)
        .set({
          sdaccesstoken: sails.config.test.accessToken
        })
        .expect(200)
        .expect(validBody)
        .end(done);
    });

    it('should search for and find a user by full name', function(done) {
      var user = sails.config.test.currentUser;

      function validBody(res) {
        res.body.length.should.be.above(0);
      }

      Request
        .get('/user/search?name=' + user.firstName + ' ' + user.lastName)
        .set({
          sdaccesstoken: sails.config.test.accessToken
        })
        .expect(200)
        .expect(validBody)
        .end(done);

    });

    it('should search for and find a user by full name and university', function(done) {
      var user = sails.config.test.currentUser;

      function validBody(res) {
        res.body.length.should.be.above(0);
      }

      Request
        .get('/user/search?name=' + user.firstName + ' ' + user.lastName + '&university=' + user.university)
        .set({
          sdaccesstoken: sails.config.test.accessToken
        })
        .expect(200)
        .end(done);

    });

  });
});
