var Request = require('supertest');
require('should');

Request = Request(process.env.API_URL);

describe('University Routes', function() {

  describe('GET /university', function() {

    it('should return an array of universities', function(done) {

      function validBody(res) {
        var body = res.body;
        body.should.be.an.instanceOf(Array);
      }

      Request
        .get('/university')
        .set({
          sdaccesstoken: sails.config.test.accessToken
        })
        .expect(200)
        .expect(validBody)
        .end(done);

    });

  });

  describe('POST /university', function() {
    it('should return an unauthorized error', function(done) {
      console.log("TODO");
      done();
    });
  });

  describe('PUT /university', function() {
    it('should return an unauthorized error', function(done) {
      console.log("TODO");
      done();
    });
  });

  describe('DELETE /university', function() {
    it('should return an unauthorized error', function(done) {
      console.log("TODO");
      done();
    });
  });

});
