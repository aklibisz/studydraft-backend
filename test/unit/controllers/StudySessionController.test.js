var Request = require('supertest'),
  Faker = require('faker');
require('should');

Request = Request(process.env.API_URL);

describe('StudySession Routes', function() {
  describe('POST /studysession/:id/associate', function() {
    it('should associate courses with a studysession', function(done) {
      var numberOfCourses = 10;

      function validBody(res) {
        res.body.courses.length.should.equal(numberOfCourses);
      }
      StudySession.create(StudySession.mock(1)[0])
        .then(function(studySession) {
          var courseIds = Course.find()
            .limit(numberOfCourses)
            .then(function(courses) {
              return courses.map(function(c) {
                return c.id;
              });
            });
          return [studySession, courseIds];
        })
        .spread(function(studySession, courseIds) {
          Request
            .post('/studysession/' + studySession.id + '/associate')
            .set('sdaccesstoken', sails.config.test.accessToken)
            .send({
              property: 'courses',
              propertyIds: courseIds
            })
            .expect(200)
            .expect(validBody)
            .end(done);
        });
    });

    it('should associate users with a studysession', function(done) {

      var studySessionMock = StudySession.mock(1)[0],
        numberOfUsers = studySessionMock.maxUsers;

      function validBody(res) {
        res.body.users.length.should.equal(numberOfUsers);
      }

      StudySession.create(studySessionMock)
        .then(function(studySession) {
          var userIds = User.find()
            .limit(numberOfUsers)
            .then(function(users) {
              return users.map(function(u) {
                return u.id;
              });
            });
          return [studySession, userIds];
        })
        .spread(function(studySession, userIds) {
          Request
            .post('/studysession/' + studySession.id + '/associate')
            .set('sdaccesstoken', sails.config.test.accessToken)
            .send({
              property: 'users',
              propertyIds: userIds
            })
            .expect(200)
            .expect(validBody)
            .end(done);
        });
    });
  });
});
