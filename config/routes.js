/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /**
   * Authentication
   */
  'POST  /auth/login': 'AuthController.login',
  'POST  /auth/register': 'UserController.create',
  'POST  /auth/login/facebook': 'AuthController.loginFacebook',
  'POST  /auth/register/facebook': 'AuthController.registerFacebook',

  /**
   * User
   */
  'GET   /user/me': 'UserController.me',
  'POST  /user/:id/associate': 'UserController.associate',
  'POST  /user/:id/disassociate': 'UserController.disassociate',
  'POST  /user/resetpassword': 'UserController.resetPassword',
  'POST  /user/:id/changepassword': 'UserController.changePassword',
  'POST  /user/:id/syncfacebook': 'UserController.syncFacebook',
  'GET   /user/search': 'UserController.search',

  /**
   * StudySession
   */
  'GET   /studysession/active': {
    controller: 'StudySession',
    action: 'find',
    where: {
      endingAt: {
        greaterThan: new Date()
      }
    },
		sort: 'endingAt ASC'
  },
  'POST  /studysession/:id/associate': 'StudySessionController.associate',
  'POST  /studysession/:id/disassociate': 'StudySessionController.disassociate',
  'POST  /studysession/:id/joinrequest/submit': 'StudySessionController.joinRequestSubmit',
  'POST  /studysession/:id/joinrequest/accept': 'StudySessionController.joinRequestAccept',
  'POST  /studySession/:id/joinrequest/reject': 'StudySessionController.joinRequestReject',

  /**
   * Device
   */
  'POST  /device/register': 'DeviceController.register',

  /**
   * Health
   */
  'POST  /health/check': 'HealthController.check'
};
