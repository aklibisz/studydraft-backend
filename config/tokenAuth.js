module.exports.tokenAuth = {
  secret: process.env.SD_TOKENAUTH_SECRET || 'aaabbbcccdddeeefffggg',
  expiresInMinutes: 43200
};
