module.exports.email = {
  service: 'Gmail',
  auth: {
    user: process.env.SD_EMAIL_AUTH_USER || 'studydraftapp@gmail.com',
    pass: process.env.SD_EMAIL_AUTH_PASS || 'Rockyt0p'
  },
  options: {
    from: 'StudyDraft Team <no-reply@studydraftapp.com>'
  }
}
