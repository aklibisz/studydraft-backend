module.exports = {

  /**
   * config/connections.js
   */
  connections: {
    default: 'mongo',
    mongo: {
      adapter: 'sails-mongo',
      url: process.env.SD_CONNECTIONS_MONGO_URL || "mongodb://studydraft-dev:Rockyt0p@ds047602.mongolab.com:47602/studydraft-dev"
    }
  },

  /**
   * config/env/development.js
   * config/env/production.js
   */
  port: process.env.SD_PORT || 1337,

  /**
   * config/environment.js
   */
  environment: process.env.SD_ENVIRONMENT || 'development',

  /**
   * config/models.js
   */
  models: {
    migrate: process.env.SD_MODELS_MIGRATE || 'alter'
  },

  /**
   * config/tokenAuth.js
   */
  tokenAuth: {
    secret: process.env.SD_TOKENAUTH_SECRET || 'aaabbbcccdddeeefffggg',
    expiresInMinutes: 43200
  },

  /**
   * config/email.js
   */
  email: {
    auth: process.env.SD_EMAIL_AUTH_USER || 'studydraftapp@gmail.com',
    pass: process.env.SD_EMAIL_AUTH_PASS || 'Rockyt0p'
  },

  /**
   * config/push.js
   */
  push: {
    sns: {
      accessKeyId: process.env.SD_PUSH_SNS_ACCESS_KEY_ID,
      secretAccessKey: process.env.SD_PUSH_SNS_SECRET_ACCESS_KEY,
      androidArn: process.env.SD_PUSH_SNS_ANDROID_ARN,
      iosArn: process.env.SD_PUSH_SNS_IOS_ARN
    }
  }
};
