var passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy;

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findOne({
    id: id
  }, function(err, user) {
    done(err, user);
  });
});

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
  },
  function(email, password, done) {

    User.findOne({
        email: email
      })
      .then(function(user) {
        if (!user) {
          return done(null, false, {
            summary: 'LocalAuth: Invalid email.',
            body: null
          });
        }

        AuthService.comparePassword(password, user.password)
          .then(function(response) {
            return done(null, user, {
              summary: 'LocalAuth: Logged in successfully.',
              body: null
            });
          })
          .catch(function(error) {
            return done(null, false, {
              summary: 'LocalAuth: Invalid password.',
              body: null
            });
          });
      })
      .catch(function(error) {
        return done(error);
      });
  }
));
