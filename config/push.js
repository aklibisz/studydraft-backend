module.exports.push = {
  sns: {
    /**
     * The id that is created when registering an AWS user via IAM with full SNS permissions
     * @type {String}
     */
    accessKeyId: process.env.SD_PUSH_SNS_ACCESS_KEY_ID,
    /**
     * The secret access key that is created along with the access key id
     * @type {String}
     */
    secretAccessKey: process.env.SD_PUSH_SNS_SECRET_ACCESS_KEY,
    /**
     * The ARN that is generated when creating a new SNS Platform Application.
     * @type {String}
     */
    androidArn: process.env.SD_PUSH_SNS_ANDROID_ARN || 'arn:aws:sns:us-east-1:790934774760:app/GCM/StudyDraftApp-Push-GCM',
    /**
     * The AWS region through which the push notification should be sent.
     * @type {String}
     */
    region: 'us-east-1'
  }
};
