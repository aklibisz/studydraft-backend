# studydraft-backend

##Status
Branch master
[![Circle CI](https://circleci.com/gh/alexklibisz/studydraft-backend/tree/master.svg?style=svg&circle-token=25d60f9de6d1ddaaf1f72bb360374a2bd2ab2676)](https://circleci.com/gh/alexklibisz/studydraft-backend/tree/master)

Branch deploy-dev
[![Circle CI](https://circleci.com/gh/alexklibisz/studydraft-backend/tree/deploy-dev.svg?style=svg&circle-token=25d60f9de6d1ddaaf1f72bb360374a2bd2ab2676)](https://circleci.com/gh/alexklibisz/studydraft-backend/tree/deploy-dev)

Branch deploy-prod
[![Circle CI](https://circleci.com/gh/alexklibisz/studydraft-backend/tree/deploy-prod.svg?style=svg&circle-token=25d60f9de6d1ddaaf1f72bb360374a2bd2ab2676)](https://circleci.com/gh/alexklibisz/studydraft-backend/tree/deploy-prod)


## Local Development

Clone the repo
> git clone https://github.com/alexklibisz/studydraft-backend  
> cd studydraft-backend

Install packages
> npm install

Run the app
> gulp

## Architecture

- Sailsjs App 
- MongoDB database hosted on MongoLab

## Deployment

See the Wiki
